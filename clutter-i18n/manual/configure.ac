m4_define([test_clutter_i18n_version], [0.0.1])

AC_PREREQ([2.63])

AC_INIT([test_clutter_i18n],
        [test_clutter_i18n_version],
        [https://malayan.collabora.co.uk/newticket],
        [test_clutter_i18n],
        [https://malayan.collabora.co.uk/])

AC_CONFIG_SRCDIR([test-clutter-i18n.c])
AC_CONFIG_MACRO_DIR([m4])

AM_INIT_AUTOMAKE([1.11 foreign -Wno-portability no-define no-dist-gzip dist-bzip2])

AM_SILENT_RULES([yes])

dnl ========================================================================

# Checks for programs.
AM_PROG_CC_C_O
AC_PROG_CXX

# require libtool >= 2.2
LT_PREREQ([2.2.6])
LT_INIT([disable-static])

# Checks for header files.
AC_HEADER_STDC

AC_CHECK_FUNCS([setlocale])
AC_CHECK_HEADERS([locale.h])

# I18n stuff
localedir="${prefix}/share/chaiwala-tests/clutter-i18n/manual/po"
AC_SUBST([localedir])
AM_GNU_GETTEXT_VERSION([0.17])
AM_GNU_GETTEXT([external])

GETTEXT_PACKAGE=test-clutter-i18n
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE,"$GETTEXT_PACKAGE",[Name of default gettext domain])

# required versions for dependencies
m4_define([glib_req_version],              [2.28.0])
m4_define([clutter_req_version],           [1.7.4])

AC_SUBST([GLIB_REQ_VERSION],     [clutter_req_version])
AC_SUBST([CLUTTER_REQ_VERSION],     [clutter_req_version])

dnl === Dependencies, compiler flags and linker libraries =====================

TEST_CLUTTER_I18N_REQUIRES="glib-2.0 >= $GLIB_REQ_VERSION clutter-1.0 >= $CLUTTER_REQ_VERSION"
PKG_CHECK_MODULES(TEST_CLUTTER_I18N, [$TEST_CLUTTER_I18N_REQUIRES])
AC_SUBST(TEST_CLUTTER_I18N_REQUIRES)

dnl ===========================================================================

AC_CONFIG_FILES([
	Makefile

	po/Makefile.in
])

AC_OUTPUT
