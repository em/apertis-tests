#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Exit nonzero if any .yaml file in the source tree is
syntactically invalid. This is run by `make check`.
"""

# Copyright © 2015 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import os
import yaml
import sys

from jinja2 import meta, \
    Environment, \
    FileSystemLoader, \
    DebugUndefined

TEMPLATES_DIR='templates'

def check_jinja2_template(filename):
    """
    This function checks the yaml format for the jinja2 templates.
    """
    env = Environment(loader=FileSystemLoader(TEMPLATES_DIR),
                      undefined=DebugUndefined)

    try:
        # First rendering is for expanding the jinja2 template macros.
        expanded_file = env.get_template(filename).render()
    except Exception as e:
        print ("Exception %s when checking %s" % (e, filename))
        sys.exit(1)


    # Let's get all variables.
    ast = env.parse(expanded_file)
    all_vars = meta.find_undeclared_variables(ast)

    # Set all variables to a placeholder '_VALUE_'
    reset_vars_values = {}
    for var in all_vars:
        reset_vars_values[var] = '_VALUE_'

    # Second rendering is for setting variables to placeholder values.
    final_yaml = env.from_string(expanded_file, globals=reset_vars_values).render()

    # Finally check yaml file syntax.
    yaml.safe_load(final_yaml)

for (dirpath, dirnames, filenames) in os.walk('.'):

    if os.path.relpath(dirpath) == TEMPLATES_DIR:
        for template in filenames:
            check_jinja2_template(template)
        continue

    for basename in filenames:
        if basename.endswith('.yaml'):
            yaml.safe_load(open(os.path.join(dirpath, basename)))
