#!/bin/sh

set -e
set -x

TMPFILE=`tempfile`

cleanup () {
	rv=$?
	set +e
	set -x

	sudo pkill -9 polkitd
	rm -f "$TMPFILE"

	# Be nice and leave polkit running as normal.
	sudo systemctl restart polkitd.service

	exit $rv
}

trap cleanup EXIT

# Restart polkitd to enable debug support (the standard systemd .service file
# starts it with --no-debug)
if [ -e /usr/lib/policykit-1/polkitd ]; then
	# Debian jessie; Ubuntu vivid; Apertis 15.03
	polkitd=/usr/lib/policykit-1/polkitd
else
	# Debian experimental as of 2015-04; Apertis 15.06
	polkitd=/usr/lib/polkit-1/polkitd
fi

( set +e; sudo ${polkitd} --replace 2>&1 ) | sed 's/^/# /' | tee $TMPFILE &

# Wait for it to start.
# FIXME: This should be replaced with something non-Telepathy-specific later.
# Like upstreaming mc-wait-for-name to gdbus.
DBUS_SESSION_BUS_ADDRESS=unix:path=/var/run/dbus/system_bus_socket \
	mc-wait-for-name org.freedesktop.PolicyKit1

# Query for actions, which forces polkit to parse all the policy files and
# output warnings if any are malformed.
#
# As a side effect, this gives a list of actions which _were_ successfully
# parsed in the test output.
#
# pkaction always seems to exit with status 1; ignore that.
pkaction || true

# Kill polkitd so the output actually gets written to TMPFILE. I have no idea
# why it doesn’t do this in the pipe.
sudo pkill -9 polkitd

# Any warnings in the polkitd output?
set +x

if grep WARNING "$TMPFILE"; then
	echo "TEST_RESULT:fail:polkit-parsing:"
	exit 1
else
	echo "TEST_RESULT:pass:polkit-parsing:"
	exit 0
fi
