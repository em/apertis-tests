#!/bin/sh

# fsck-boot-rootfs.sh
#
# Check if fsck has been started for rootfs
#
# In case if fsck has been started it savees 2 files in directory
# '/run/initramfs':
# - stamp file 'fsck-root' in case if fsck run was successful
# - log file 'fsck.log' with filesystem check log
#
# Since fsck is started on every boot it is enough to make sure we have
# root file system check on initramfs stage.

# Copyright © 2019 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


set -e

if test -s /run/initramfs/fsck.log; then
    echo "fsck-boot-log: pass"
else
    echo "fsck-boot-log: fail"
    exit 1
fi

if test -f /run/initramfs/fsck-root; then
    echo "fsck-boot-root: pass"
else
    echo "fsck-boot-root: fail"
    exit 1
fi

exit 0
