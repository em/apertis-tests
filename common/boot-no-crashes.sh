#!/bin/sh

# boot-no-crashes.sh
#
# Verify that no processes crashed so far during this boot. The journal is
# automatically attached to LAVA test failures, so we don’t need to manually
# include that.

# Copyright © 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


set -e

echo "# Anomalous process terminations:"
sudo journalctl -b | grep ANOM_ABEND | sed 's/^/# /g'

if sudo journalctl -b | grep -q ANOM_ABEND; then
    echo "TEST_RESULT:fail:boot-no-crashes:ANOM_ABEND found in journalctl logs" >&2
    exit 1
else
    echo "TEST_RESULT:pass:boot-no-crashes:"
    exit 0
fi
