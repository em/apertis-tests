#
# This file contains the variables and commands for the persistent disk test.
#

# Variables for running the test.
# These can be easily changed to fit other type of configurations.

from subprocess import check_call, check_output, CalledProcessError
import os
import time


class PsdkCommands(object):

    def __init__(self, old_sdk, new_sdk, user_test_file, debug):
        self.vbox_manage = "/usr/bin/vboxmanage"
        self.ssh_id_file = "ssh/sdk_id_rsa"
        self.local_host = "user@127.0.0.1"
        self.local_port = "2222"
        self.old_sdk = old_sdk
        self.new_sdk = new_sdk
        self.user_test_file = user_test_file
        self.debug = debug

    def _run(self, *commands, ssh=False, output=False):
        # Try to handle the case of connection reset errors on ssh
        # by calling several times the command if it fails.
        def _run_command(cmd, ssh, output, attempts=3):
            try:
                if output:
                    return check_output(cmd)
                else:
                    check_call(cmd)
            except CalledProcessError:
                if ssh and attempts > 0:
                    print("Trying again in 3 secs ...")
                    time.sleep(3)
                    _run_command(cmd, ssh, output, attempts - 1)
                else:
                    print("FAILED: %s" % cmd)
                    exit(1)
            except OSError:
                print("FAILED: Error running command: %s" % cmd)
                exit(1)

        for cmd in commands:
            if self.debug:
                print(cmd)
            if output:
                return _run_command(cmd, ssh, output)
            else:
                _run_command(cmd, ssh, output)

    def _ssh(self, lst=[], copy_id=False, copy_file=False):
        ssh_opts = [ "-o", "StrictHostKeyChecking=no",
                     "-o", "UserKnownHostsFile=/dev/null",
                     "-o", "LogLevel=ERROR" ]
        if copy_file:
            return [ "scp", "-i", self.ssh_id_file,
                     "-P", self.local_port ] + ssh_opts + lst
        else:
            cmd = "ssh-copy-id" if copy_id else "ssh"
            return [ cmd, self.local_host, "-i", self.ssh_id_file, "-p",
                     self.local_port ] + ssh_opts + lst

    def create_vm(self, vm):
        print("Creating Initial VM (%s) ..." % vm)
        self._run(
            [ self.vbox_manage, "createvm", "--name", vm, "--ostype",
              "Debian64", "--register" ],
            [ self.vbox_manage, "modifyvm", vm, "--memory", "2048",
              "--firmware", "efi64", "--nic1", "nat" ],
            [ self.vbox_manage, "storagectl", vm, "--name",
              '"SATA Controller"', "--add", "sata", "--bootable", "on" ],
            [ self.vbox_manage, "modifyvm", vm, "--natpf1",
              ("apertis,tcp,,%s,,22" % self.local_port) ]
        )

    def start_vm(self, vm, vmtype='headless'):
        self._run(([self.vbox_manage, "startvm", vm, "--type", vmtype]))

    def clone_vm(self, old_vm, new_vm):
        print("Cloning %s into %s ..." % (old_vm, new_vm))
        self._run([ self.vbox_manage, "clonevm", old_vm, "--name",
                    new_vm, "--mode", "all", "--register" ])

    def attach_vdi(self, vm, vdi_path):
        print("Attaching Disk %s ..." % vdi_path)
        self._run([ self.vbox_manage, "storageattach", vm, "--storagectl",
                    '"SATA Controller"', "--port", "0", "--device", "0",
                    "--type", "hdd", "--medium", vdi_path ])

    def create_persistent_disk(self, pdisk_path):
        print("Creating persistent disk: %s" % pdisk_path)
        self._run([ self.vbox_manage, "createhd", "--size", "40000", "--variant",
                    "Standard", "--filename", pdisk_path ])

    def attach_persistent_disk(self, vm, pdisk_path):
        self._run([ self.vbox_manage, "storageattach", vm, "--storagectl",
                    '"SATA Controller"', "--port", "1", "--device", "0",
                    "--type", "hdd", "--medium", pdisk_path ])

    def poweroff_vm(self, vm):
        self._run([ self.vbox_manage, "controlvm", vm, "acpipowerbutton" ])
        if self.debug:
            print("Waiting 5 secs ...")
        time.sleep(5)

    def run_ade_sysroot(self, release, op='update'):
        self._run(self._ssh([ "ade", "sysroot", op, "--release", release ]),
                  ssh=True)

    def copy_file(self, cfile):
        self._run(self._ssh([cfile, (self.local_host + ":/home/user/") ],
                            copy_file=True), ssh=True)

    def move_proxy_file(self, proxy_test_file, apt_conf_dir):
        self._run(self._ssh(["sudo", "mv",
                             os.path.join("/home/user/", proxy_test_file),
                             apt_conf_dir]), ssh=True)

    def check_file(self, file_path):
        print(f"\nChecking file contents for {file_path}:\n" \
              "===========================================================")
        self._run(self._ssh([ "cat", file_path ]), ssh=True)
        print("===========================================================")

    def psdk_add_file(self, file_path):
        self._run(self._ssh([ "psdk", "-e", file_path ]), ssh=True)

    def delete_vm(self, vm):
        print("Deleting VM %s ..." % vm)
        self._run([ self.vbox_manage, "unregistervm", vm, "--delete" ])

    def copy_ssh_key(self):
        self._run(self._ssh(copy_id=True), ssh=True)

    def clean(self):
        print("Cleaning environment ...")
        if os.path.isfile(self.user_test_file):
            print("Removing", self.user_test_file)
            os.remove(self.user_test_file)

        if self.is_vm_running(self.new_sdk):
            print("Stopping VM:", self.new_sdk)
            self.poweroff_vm(self.new_sdk)
        if self.is_vm_running(self.old_sdk):
            print("Stopping VM:", self.old_sdk)
            self.poweroff_vm(self.old_sdk)

        self.delete_vm(self.old_sdk)
        self.delete_vm(self.new_sdk)

    def is_vm_running(self, vm):
        vms = self._run([self.vbox_manage, "list", "runningvms"],
                        output=True).decode('utf-8')
        if self.debug:
            print(vms)
        for v in vms.split('\n'):
            lst = v.split()
            if len(lst) > 0:
                if lst[0] == ('"'+vm+'"'):
                    return True
        return False

    def set_ssh_key(self):
        # Make sure SSH key file has right permissions
        os.chmod(self.ssh_id_file, 0o600)
