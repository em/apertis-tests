#!/bin/sh

##############################
# R1.17 sdk-roller-widget
#
# This runs a roller demo for a given amount of time. It automatically slides
# the widget around to simulate constant usage.
#
# At the end, this program prints the average frame rate as reported by Clutter
##############################

fps_file=/tmp/fps.out
duration=60
export CLUTTER_SHOW_FPS=1

Xephyr :1 -host-cursor -screen 800x480x24 -ac >/dev/null 2>&1 &
xephyr_pid=$!
export DISPLAY=:1
mutter >/dev/null 2>&1 &
mutter_pid=$!

cd ~/roller-mx/examples/

echo "Please wait $duration seconds for this test to run and terminate."

# Run the demo for some given time
(./basic --autoscroll > $fps_file ) &
cmd_pid=$!
sleep $duration
kill -s SIGTERM $cmd_pid $xephyr_pid $mutter_pid

# filter out lines truncated by surprise kill above
valid_line='ClutterStage.*\*\*\*'

# strip lines to bare fps reading
readings=$(cat $fps_file | grep  $valid_line | sed 's/.* \([0-9]\+\) .*/\1/')

# calculate average reading
readings_num=0
for i in $readings; do
        readings_num=$(($readings_num + 1))
        readings_total=$((readings_total + $i))
done
fps_mean=$(($readings_total / $readings_num))

echo "Average frames per second (over $duration seconds): $fps_mean"

# clean up
rm $fps_file
