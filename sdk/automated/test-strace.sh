#!/bin/bash

# This smoke-checks strace by tracing a simple program and ensuring that the
# strace output matches some basic assumptions

TEST_PROG=echo
TEST_PROG_ARGS="tracing a simple command"

die() {
        if [ "x$1" != "x" ]; then
                echo -e "$1"
        fi
        echo "sdk-debug-tools-strace-smoke-test: FAILED"
        exit 0
}

#
# Clean-up from previous runs
#
rm -f $TEST_PROG

#
# Main test
#
STRACE_ARRAY_EXPECTED=(
"^exec.*(\"[A-Za-z0-9/]*$TEST_PROG\""
"^write.*(1, \"$TEST_PROG_ARGS"
)

strace_array_missing=()
strace_output="$(strace $TEST_PROG $TEST_PROG_ARGS 2>&1)"

# check that every expected line is present in the strace output
matches=0
for i in $(seq 0 $((${#STRACE_ARRAY_EXPECTED[@]} - 1))); do
        if [ "x$(echo "$strace_output" | grep "${STRACE_ARRAY_EXPECTED[$i]}" )" != "x" ]; then
                matches=$(($matches + 1))
                continue
        fi

        strace_array_missing+=("${STRACE_ARRAY_EXPECTED[$i]}")
done

STATUS_STR="FAILED"
if [ $matches -eq ${#STRACE_ARRAY_EXPECTED[@]} ]; then
        STATUS_STR="PASSED"
else
        for n in ${strace_array_missing[@]}; do
                missing="$missing\n$n"
        done
        die "Expected patterns missing in strace output:\n$missing\n"
fi

echo "sdk-debug-tools-strace-smoke-test: $STATUS_STR"
