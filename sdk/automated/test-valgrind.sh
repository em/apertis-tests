#!/bin/sh

# This smoke-checks valgrind by testing a sample program for memory leaks
TEST_DIR=$(dirname $0)
TEST_PROG=$(mktemp)
TEST_SRC_NAME=leak

die() {
        if [ -n "$1" ]; then
                /bin/echo -e "$1"
        fi
        echo "sdk-debug-tools-valgrind-smoke-test: FAILED"
        exit 0
}

#
# Build program with gprof support
#
cc -g -o $TEST_PROG $TEST_DIR/$TEST_SRC_NAME.c

#
# Main test
#

# ensure no leak
valgrind_out=$(valgrind --error-exitcode=1 --leak-check=yes $TEST_PROG 2>&1)
if [ $? -ne 0 ]; then
        errmsg="Sample program leaked memory unexpectedly"
        die "$valgrind_out\n\n$errmsg"
fi

# ensure there is a leak
valgrind_out=$(valgrind --error-exitcode=1 --leak-check=yes $TEST_PROG --leak 2>&1)
if [ $? -eq 0 ]; then
        errmsg="Sample program memory leak undetected"
        die "$valgrind_out\n\n$errmsg"
fi

# Clean temporary program
rm -f $TEST_PROG

echo "sdk-debug-tools-valgrind-smoke-test: PASSED"
