#!/bin/bash

set -eEu

trap 'echo ; echo flatpak-demo-test: fail' ERR

flatpak --user remote-add --if-not-exists --no-gpg-verify apertis-runtime https://images.apertis.org/flatpak/runtime
flatpak --user remote-add --if-not-exists --no-gpg-verify apertis-app https://images.apertis.org/flatpak/app

flatpak install -y apertis-app org.apertis.demo.curl

flatpak run --share=network org.apertis.demo.curl https://www.apertis.org

echo
echo flatpak-demo-test: pass
